// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app)
var usernames = {};
var numUsers=0;
var lobby={room:"Lobby", banned:[]};
var rooms = [];
rooms[0]=lobby;
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
 
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: "+data["message"]); // log it to the Node.JS output
		io.sockets.to(socket.room).emit("message_to_client",{message: data["message"],username:socket.username }) // broadcast the message to other users
	});
	socket.on('message_private', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: "+data["message"]); // log it to the Node.JS output
		usernames[data["userpriv"]].emit("message_to_client",{message: "sent PM: " + data["message"],username:socket.username }) // broadcast the message to other users
	});
	socket.on('add_user', function(data) {
		socket.username=data["message"];
		usernames[socket.username] = socket;
		socket.room="Lobby";
		socket.join('Lobby');
		var msg = "You have connected to Lobby!";
		socket.emit("message_to_client1",{message:msg})
		io.sockets.to('Lobby').emit("message_to_client1",{message:data["message"] + " has joined Lobby."})
		numUsers++;

	});
	socket.on('create_room',function(data) {
		socket.leave(socket.room);
		io.sockets.to(socket.room).emit("message_to_client1",{message:socket.username + " has left " + socket.room})
		socket.room=data["message"];
		var newRoom = {room:socket.room,password:data["password"],creator:socket.username,banned:[]};
		rooms.push(newRoom);
		var msg = "You have connected to and are the creator of " + socket.room + "!";
		socket.join(socket.room);
		socket.emit("message_to_client1",{message:msg})
		});
	socket.on('change_room',function(data){
		var exists = false;
		var roomLength = rooms.length;
		for(i=0;i<roomLength;i++){
			if (rooms[i].room==data["message"]) {
				exists=true;
				break;
			}
		}
		if (exists) {
			var banned = false;
			for(j=0;j<rooms[i].banned.length;j++){
				if (rooms[i].banned[j]==socket.username) {
					banned = true;
				}
			}
		if (banned) {
			socket.emit("message_to_client1",{message:"Your are banned from this room"})
		}
		else if ((rooms[i].password!=null && rooms[i].password==data["password"]) || rooms[i].password==null) {
		socket.leave(socket.room);
		io.sockets.to(socket.room).emit("message_to_client1",{message:socket.username + " has left " + socket.room})
		socket.room=data["message"];
		var msg = "You have connected to " + socket.room + "!";
		io.sockets.to(socket.room).emit("message_to_client1",{message:socket.username + " has joined " + socket.room})
		socket.join(socket.room);
		socket.emit("message_to_client1",{message:msg})
		}
		else{
			var msg="Incorrect Password"
			socket.emit("message_to_client1",{message:msg})
		}
		}
		else{
			var msg="Room does not exist."
			socket.emit("message_to_client1",{message:msg})
		}
		});
	socket.on('kick_user', function(data){
		var userkick=data["message"];
		for(i=0;i<rooms.length;i++){
			if (rooms[i].room==socket.room) {
				break;
			}
		}
		if (rooms[i].creator==socket.username) {
			usernames[userkick].leave(socket.room);
			usernames[userkick].join('Lobby');
			usernames[userkick].room="Lobby";
			usernames[userkick].emit("message_to_client1",{message: "You have been kicked from room " + socket.room})
			io.sockets.to(socket.room).emit("message_to_client1",{message:userkick + " has been kicked"})
		}
		else{
			var msg = "You are not the creator";
		socket.emit("message_to_client1",{message:msg})
		}
	});
	socket.on('ban_user', function(data){
		var userban = data["message"];
		for(i=0;i<rooms.length;i++){
			if (rooms[i].room==socket.room) {
				break;
			}
		}
		if (rooms[i].creator==socket.username) {
		    if (usernames[userban].room==socket.room){
			rooms[i].banned.push(userban);
			usernames[userban].leave(socket.room);
			usernames[userban].join('Lobby');
			usernames[userban].room="Lobby";
			usernames[userban].emit("message_to_client1",{message: "You have been banned from room " + socket.room})
			io.sockets.to(socket.room).emit("message_to_client1",{message:userban + " has been kicked"})
		    }
		    else{
			rooms[i].banned.push(userban);
			io.sockets.to(socket.room).emit("message_to_client1",{message:userban + " has been kicked"})
		    }
		}
		else{
			var msg = "You are not the creator";
		socket.emit("message_to_client1",{message:msg})
		}
	});
	});